package com.zuitt.wdc044_test.services;

import com.zuitt.wdc044_test.models.User;

import java.util.Optional;
public interface UserService {
    void createUser(User user);
    Optional<User> findByUsername(String username);
}