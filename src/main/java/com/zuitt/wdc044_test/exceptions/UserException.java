package com.zuitt.wdc044_test.exceptions;

public class UserException extends Exception{
    public UserException(String message){
        super(message);
    };
}
